@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <p><a href="/note/create">+ Create a note</a></p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Название заметки</th>
                                <th>Содержимое заметки</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($notes as $index => $note)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ $note->title }}</td>
                                    <td>{{ $note->content }}</td>
                                    <td>
                                        <a href="{{ action('NoteController@edit', $note->id) }}">Редактировать</a>
                                    </td>
                                    <td>
                                        <form class="confirm-delete" role="form" method="POST" action="{{ action('NoteController@destroy', $note->id) }}">
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn btn-link" type="submit">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
