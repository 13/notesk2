@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Заметки</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ action('NoteController@update', $note->id) }}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="_method" value="put">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Название</label>
                            <div class="col-md-8">
                                <input class="form-control" name="title" value="{{ $note->title }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Содержание</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="content">{{ $note->content }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
