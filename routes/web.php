<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/note/create', 'NoteController@create');
Route::post('/note/store', 'NoteController@store');
Route::get('/note/show', 'NoteController@show');
Route::get('/note/edit/{id}', 'NoteController@edit');
Route::put('/note/update/{id}', 'NoteController@update');
Route::delete('/note/destroy/{id}', 'NoteController@destroy');
