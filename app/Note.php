<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Note extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'user_id'
    ];

    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    public function scopePermitted($query)
    {
        $query->where('user_id', Auth::user()->id);
    }
}
