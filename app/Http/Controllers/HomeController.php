<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::permitted()->get();
        return view('home', array('notes' => $notes));
    }
}
